from flask import Flask
import os

app = Flask(__name__)

@app.route("/")
def wish():
    message = "OM {name} NAMAHA"
    return message.format(name=os.getenv("NAME", "SHIVAYA"))
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
